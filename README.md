
RHEL 7 / CentOS 7에서는 iptables를 관리하기 위해 [firewalld](https://fedoraproject.org/wiki/FirewallD) 가 도입되었습니다. IMHO, firewalld는 서버 환경보다 워크 스테이션에 더 적합합니다.

보다 고전적인 iptables 설정으로 돌아갈 수 있습니다. 먼저 firewalld 서비스를 중지하고 마스크하십시오.

```
systemctl stop firewalld
systemctl mask firewalld

```

그런 다음 iptables-services 패키지를 설치하십시오.

```
yum install iptables-services

```

부팅시 서비스 활성화 :

```
systemctl enable iptables

```

서비스 관리

```
systemctl [stop|start|restart] iptables

```

방화벽 규칙을 저장하는 방법은 다음과 같습니다.

```
service iptables save

```

또는

```
/usr/libexec/iptables/iptables.init save
```